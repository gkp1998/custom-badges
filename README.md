# Custom badges for GitLab CI

Vanilla GiLab badge: [![pipeline status](https://gitlab.com/kubouch/ci-test/badges/badges/pipeline.svg)](https://gitlab.com/kubouch/ci-test/pipelines)

Dropbox badges:
* Success job: [![Dropbox fail](https://www.dropbox.com/s/wht94wlcg1yt8pf/ci-test-badges-success_job.svg?raw=1)](https://www.dropbox.com/s/wht94wlcg1yt8pf/ci-test-badges-success_job.svg?dl=0)
* Failing job: [![Dropbox fail](https://www.dropbox.com/s/6dlgfztxh4pfkwr/ci-test-badges-failure_job.svg?raw=1)](https://www.dropbox.com/s/6dlgfztxh4pfkwr/ci-test-badges-failure_job.svg?dl=0)

## Overview

This is a how-to repo about making custom badges during the GitLab CI.
In this examlple I use it to create a separate badge for each job.
The basic idea is to fetch a badge from [shields.io](https://shields.io) during the CI process and upload it somewhere (Dropbox) from where it can be directly referenced.
In this example, Dropbox is used as is allows to upload files via HTTP requests without the need of a client installation.
Other cloud services (Google Drive, MEGA, OneDrive, ...) might allow it, too.
The advantage of this approach is that you don't have to set up your own badge server and it has minimal requirements (you just need to be able to make a HTTP request somehow).

## Prerequisites

* Dropbox account (**no need** for an installed client)
* Any means to execute a HTTP GET and POST requests (e.g. curl or Python [requests](https://github.com/requests/requests) library)
* Some GitLab CI workflow

## How To

### Preparation:

1. In the Dropbox API Explorer find an [upload](https://dropbox.github.io/dropbox-api-v2-explorer/#files_upload) app.
Click `Get Token` on the top.

2. Save the token as a [secret variable](https://docs.gitlab.com/ce/ci/variables/README.html#secret-variables) (DBOX_TOKEN in this case) in your project settings.

### The Flow:

1. Save your custom badge from [shields.io](https://shields.io) into a file. Example:
```
curl https://img.shields.io/badge/test-it-ff69b4.svg > test-it.svg
```

2. Upload the badge via an HTTP request to the Dropbox API.
The request command can ge generated in the [upload app](https://dropbox.github.io/dropbox-api-v2-explorer/#files_upload) by filling the fields and clicking `Show Code` at the bottom.
I used a curl request but there are more options including Python [requests](https://github.com/requests/requests).
It is **important** that `autorename` is set to `false` and `mode` to `overwrite` (see [documentation](https://www.dropbox.com/developers/documentation/http/documentation#files-upload))!
It can look like this:
```
curl -X POST https://content.dropboxapi.com/2/files/upload \
  --header 'Authorization: Bearer $DBOX_TOKEN' \
  --header 'Content-Type: application/octet-stream' \
  --header 'Dropbox-API-Arg: {"path":"/Photos/badges/test-it.svg","autorename":false,"mute":false,"mode":{".tag":"overwrite"}}' \
  --data-binary @'test-it.svg'
```
This command uploads the downloaded badge  `test-it.svg` to Dropbox with a Dropbox path `/Photos/badges/test-it.svg`.

### Reference the Badge:

1. Sign into your Dropbox account and create a shareable link to the newly uploaded file.
 
2. **Important!** Shareable link from Dropbox ends with `?dl=0`.
Replace it with `?raw=1` ([reference](https://cantonbecker.com/etcetera/2014/how-to-directly-link-or-embed-dropbox-images)).
Also don't try to right click the image and copy the image URL.
That works for a while but the link is temporary and will break after a few hours.

3. Embed the `?raw=1`-ending link in your README or wherever you want to publish the badge.

## Custom Badge for Each Job (`.gitlab-ci.yml` example)

You need to use your own creativity depending on what you need.
In this example, I use the following flow to get a separate badge for each job:
```yaml
job_name:
    stage: some_stage
    before_script:
        # Create a 'running' badge and upload it to Dropbox
    script:
        # 1. Write 'failing' to a file.
        # 2. Run the job!
        # 3. Write 'success' to the file.
    after_script:
        # Create and upload a badge depending on the status in the file.
```
First, the badge is set to `running` before running the job.
This is done in `before_script`.
You can as well skip it if you don't it.
Then, right before the job, set the write the status as 'failing' and execute the job.
After the job, set the status to 'success'.
If there is a failure, this will not get executed and status is still 'failing'.
`after_script` gets executed even if the job fails.
It evaluates the status, creates an appropriate badge and uploads it.
The implementation details are in [.gitlab-ci.yml](https://gitlab.com/kubouch/ci-test/blob/badges/.gitlab-ci.yml) and [update_badge.sh](https://gitlab.com/kubouch/ci-test/blob/badges/update_badge.sh) but you probably want to tailor it to your own needs.



