#!/bin/sh

# 'running', 'success' or 'failure' is in this file
STATUS=`cat status.txt`

# Set values for shields.io fields based on STATUS
if [ $STATUS = "running" ]; then
	BADGE_STATUS="build"
	BADGE_SUBJECT="running"
	BADGE_COLOR="yellow"
elif [ $STATUS = "failure" ]; then
	BADGE_STATUS="build"
	BADGE_SUBJECT="failed"
	BADGE_COLOR="red"
elif [ $STATUS = "success" ]; then
	BADGE_STATUS="build"
	BADGE_SUBJECT="passlled"
	BADGE_COLOR="brightgreen"
else
	exit 1
fi

# Set filename for the badge (i.e. 'ci-test-branch-job.svg')
BADGE_FILENAME="ci-test-$CI_COMMIT_REF_NAME-$CI_JOB_NAME.svg"

# Get the badge from shields.io
echo "INFO: Fetching badge $SHIELDS_IO_NAME from shields.io to $BADGE_FILENAME."
SHIELDS_IO_NAME=$BADGE_STATUS-$BADGE_SUBJECT-$BADGE_COLOR.svg
curl "https://img.shields.io/badge/$SHIELDS_IO_NAME" > $BADGE_FILENAME

# Upload it to Dropbox (DBOX_TOKEN and DBOX_BADGES_PATH are secret CI variables)
echo "INFO: Uploading badge to Dropbox."
DBOX_PATH="$DBOX_BADGES_PATH/ci-test"
curl -s -X POST https://content.dropboxapi.com/2/files/upload \
  --header "Authorization: Bearer $DBOX_TOKEN" \
  --header "Content-Type: application/octet-stream" \
  --header "Dropbox-API-Arg: {\"path\":\"$DBOX_PATH/$BADGE_FILENAME\",\"autorename\":false,\"mute\":false,\"mode\":{\".tag\":\"overwrite\"}}" \
  --data-binary @"$BADGE_FILENAME"
